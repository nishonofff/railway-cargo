package com.example.railwaycargo.components;

import android.annotation.SuppressLint;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.widget.EditText;


/**
 * @author Akbar Begimkulov
 * @since 2019, January 28
 */

public class MaskTextWatcher implements TextWatcher {

    private final EditText mEditText;
    private final String mMask;
    private final int mMaskLength;
    private final int mNoSpaceMaskLength;
    private int mPosition;
    private OnServiceTypeListener mServiceTypeListener;
    private OnWrongFormatListener mWrongFormatListener;
    private OnWrongDateFormatListener mWrongDateFormatListener;
    private TextInputLayout mTextInputLayout;
    private OnClearErrorListener mClearErrorListener;
    private OnTextReadyListener mTextReadyListener;
    private OnTextNotReadyListener mTextNotReadyListener;
    private String mPrefix;
    private int mDeleteKeyCode = 0;
    private boolean mIsErrorCalled = false;
    private MaskTextWatcher mMaskTextWatcher;

    private MaskTextWatcher(EditText editText, String mask, int position) {
        mEditText = editText;
        mMask = mask;
        mPosition = position;

        mMaskLength = mask.length();
        mNoSpaceMaskLength = mask
                .replaceAll("\\.", "")
                .replaceAll("\\s", "").length();
        setupPrefix(mask);
        mEditText.setText(mPrefix);
        Selection.setSelection(mEditText.getText(), mEditText.getText().length());
        mEditText.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_DEL) mDeleteKeyCode = 1;
            return false;
        });
        mMaskTextWatcher = this;
    }

    private void setServiceTypeListener(OnServiceTypeListener serviceTypeListener) {
        mServiceTypeListener = serviceTypeListener;
    }

    private void setWrongFormatListener(OnWrongFormatListener maskFormatListener) {
        mWrongFormatListener = maskFormatListener;
    }

    private void setWrongDateFormatListener(OnWrongDateFormatListener maskDateListener) {
        mWrongDateFormatListener = maskDateListener;
    }

    private void setClearErrorListener(TextInputLayout inputLayout, OnClearErrorListener clearErrorListener) {
        mTextInputLayout = inputLayout;
        mClearErrorListener = clearErrorListener;
    }

    private void setTextReadyListener(OnTextReadyListener textReadyListener) {
        mTextReadyListener = textReadyListener;
    }

    private void setTextNotReadyListener(OnTextNotReadyListener textNotReadyListener) {
        mTextNotReadyListener = textNotReadyListener;
    }

    private void setupPrefix(String mask) {
        int startIndex1 = mask.indexOf(" #");
        int startIndex2 = mask.indexOf("#");
        int startIndex = startIndex1 < startIndex2 ? startIndex1 : startIndex2;
        if (startIndex > 0) {
            mPrefix = mask.substring(0, startIndex);
        } else {
            mPrefix = "";
        }
    }

    @Override
    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        mIsErrorCalled = false;
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (mMaskLength == 0) {
            return;
        }

        String noMaskText = mEditText.getText().toString()
                .replaceAll("\\.", "")
                .replaceAll("\\s", "");

        if (!mPrefix.isEmpty()) {
            String noMaskPrefix = mPrefix
                    .replaceAll("\\.", "")
                    .replaceAll("\\s", "");
            if (noMaskText.startsWith(noMaskPrefix) && noMaskText.length() > noMaskPrefix.length()) {
                String tempText = noMaskText.substring(noMaskPrefix.length());
                boolean isDoublePrefix = tempText.startsWith(noMaskPrefix)
                        && tempText.length() == mNoSpaceMaskLength;

                boolean isDoubleNoSignPrefix = noMaskPrefix.startsWith("+")
                        && noMaskPrefix.length() > 1
                        && tempText.length() > 1
                        && tempText.startsWith(noMaskPrefix.substring(1))
                        && tempText.length() == mNoSpaceMaskLength - 1;

                if (isDoublePrefix) {
                    noMaskText = tempText;
                } else if (isDoubleNoSignPrefix) {
                    noMaskText = "+" + tempText;
                }
            }
        }

        int length = mEditText.getText().toString().length();
        int cursor = mEditText.getSelectionStart();
        int noMaskTextLength = noMaskText.length();

        if (noMaskTextLength > mNoSpaceMaskLength) {
            setText(noMaskText, length, cursor);
            if (mWrongFormatListener != null) {
                mIsErrorCalled = true;
                mWrongFormatListener.onWrongFormat(mPosition);
            }
            return;
        }

        StringBuilder formattedTextBuilder = new StringBuilder();

        char[] maskSymbols = mMask.toCharArray();
        for (int n = 0, m = 0; n < noMaskTextLength && m < mMaskLength; m++) {
            if (maskSymbols[m] == ' ') {
                formattedTextBuilder.append(' ');
            } else if (maskSymbols[m] == '.') {
                formattedTextBuilder.append('.');
            } else if (maskSymbols[m] == '#') {
                if (noMaskText.charAt(n) >= '0' && noMaskText.charAt(n) <= '9') {
                    formattedTextBuilder.append(noMaskText.charAt(n));
                } else if (noMaskText.charAt(n) == '*') {
                    formattedTextBuilder.append('*');
                } else {
                    m--;
                }
                n++;
            } else {
                formattedTextBuilder.append(maskSymbols[m]);
                n++;
            }
        }


        checkDateValue(formattedTextBuilder.toString());

        setText(formattedTextBuilder.toString(), length, cursor);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void afterTextChanged(Editable editable) {
        if (!mIsErrorCalled && mClearErrorListener != null && mTextInputLayout != null) {
            mClearErrorListener.onClearInputFieldError(mTextInputLayout);
        }

        if (mPrefix.length() == 0) return;
        if (!editable.toString().startsWith(mPrefix)) {
            mEditText.setText(mPrefix);
            Selection.setSelection(mEditText.getText(), mEditText.getText().length());
        }
    }

    private void checkDateValue(String value) {
        if (mWrongDateFormatListener != null) {
            int valueLength = value.length();
            if (valueLength == 1) {
                if (value.equals("0")) return;
                if (value.equals("1")) return;
                if (value.equals("2")) return;
                if (value.equals("3")) return;
                mIsErrorCalled = true;
                mWrongDateFormatListener.onWrongDate(mPosition);
                return;
            }
            if (valueLength == 10) {
                int year = Integer.parseInt(value.substring(6));
                if (!(year >= 1000 && year < 3000)) {
                    mIsErrorCalled = true;
                    mWrongDateFormatListener.onWrongDate(mPosition);
                    return;
                }
            } else if (valueLength == 7) {
                if (!(value.charAt(6) != '1' || value.charAt(6) != '2')) {
                    mIsErrorCalled = true;
                    mWrongDateFormatListener.onWrongDate(mPosition);
                    return;
                }
            } else if (valueLength == 4) {
                if (!(value.charAt(3) == '0' || value.charAt(3) == '1')) {
                    mIsErrorCalled = true;
                    mWrongDateFormatListener.onWrongDate(mPosition);
                    return;
                }
            }

            if (valueLength >= 2) {
                int day = Integer.parseInt(value.substring(0, 2));
                if (day <= 0 || day > 31) {
                    mIsErrorCalled = true;
                    mWrongDateFormatListener.onWrongDate(mPosition);
                    return;
                }
                if (valueLength >= 5) {
                    int month = Integer.parseInt(value.substring(3, 5));
                    if (month <= 0 || month > 12) {
                        mIsErrorCalled = true;
                        mWrongDateFormatListener.onWrongDate(mPosition);
                    }
                }
            }
        }
    }

    private void setText(String text, int length, int cursor) {
        mEditText.removeTextChangedListener(mMaskTextWatcher);
        mEditText.setText(text);
        if (length == cursor) {
            mEditText.setSelection(mEditText.getText().toString().length());
        } else {
            mEditText.setSelection(cursor);
        }
        mEditText.addTextChangedListener(mMaskTextWatcher);
    }

    public interface OnServiceTypeListener {

        void onServiceType(int position);
    }

    public interface OnWrongFormatListener {

        void onWrongFormat(int position);
    }

    public interface OnWrongDateFormatListener {

        void onWrongDate(int position);
    }

    public interface OnClearErrorListener {

        void onClearInputFieldError(TextInputLayout inputLayout);
    }

    public interface OnTextReadyListener {

    }

    public interface OnTextNotReadyListener {

        void onTextNotReady(int position);
    }

    public static class Builder {

        private MaskTextWatcher mMaskTextWatcher;

        public Builder(EditText editText, String mask, int position) {
            mMaskTextWatcher = new MaskTextWatcher(editText, mask, position);

        }

        public Builder setServiceTypeListener(OnServiceTypeListener serviceTypeListener) {
            mMaskTextWatcher.setServiceTypeListener(serviceTypeListener);
            return this;
        }

        public Builder setWrongFormatListener(OnWrongFormatListener maskFormatListener) {
            mMaskTextWatcher.setWrongFormatListener(maskFormatListener);
            return this;
        }

        public Builder setClearErrorListener(TextInputLayout inputLayout, OnClearErrorListener clearErrorListener) {
            mMaskTextWatcher.setClearErrorListener(inputLayout, clearErrorListener);
            return this;
        }

        public Builder setTextReadyListener(OnTextReadyListener textReadyListener) {
            mMaskTextWatcher.setTextReadyListener(textReadyListener);
            return this;
        }

        public Builder setTextNotReadyListener(OnTextNotReadyListener textNotReadyListener) {
            mMaskTextWatcher.setTextNotReadyListener(textNotReadyListener);
            return this;
        }

        public Builder setMaskDateListener(OnWrongDateFormatListener maskDateListener) {
            mMaskTextWatcher.setWrongDateFormatListener(maskDateListener);
            return this;
        }

        public MaskTextWatcher build() {
            return mMaskTextWatcher;
        }
    }
}