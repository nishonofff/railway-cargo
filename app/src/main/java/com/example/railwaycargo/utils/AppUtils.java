package com.example.railwaycargo.utils;

import android.annotation.SuppressLint;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AppUtils {

    private static String mLocale = "ru";

    @SuppressLint("SimpleDateFormat")
    public static String formatDateToLocal(String date) {
        DateFormat inDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outDateFormat = new SimpleDateFormat("dd MMM , yyyy", new Locale(mLocale));
        try {
            Date tempDate = inDateFormat.parse(date);
            return outDateFormat.format(tempDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
