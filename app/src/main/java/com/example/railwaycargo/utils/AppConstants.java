package com.example.railwaycargo.utils;

public interface AppConstants {
    /**
     * Network timeouts
     */
    byte CONNECT_TIMEOUT = 120;
    byte WRITE_TIMEOUT = 120;
    byte READ_TIMEOUT = 120;

    String BASE_API_URL = "http://84.54.96.90:2224/api/";


}
