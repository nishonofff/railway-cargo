package com.example.railwaycargo.data.network.entity;

public class RequestBody {
    public String inn;
    public String singKey;
    public String serviceId;

    @Override
    public String toString() {
        return "{" +
                '\"'+"inn"+'\"'+":" + '\"'+ inn + '\"' +","+
                '\"'+"singKey"+'\"'+":" + '\"'+ singKey + '\"' +","+
                '\"'+"serviceId"+'\"'+":" + '\"'+ serviceId + '\"'+
                '}';
    }
}
