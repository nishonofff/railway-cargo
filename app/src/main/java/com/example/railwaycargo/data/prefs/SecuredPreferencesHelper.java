package com.example.railwaycargo.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

class SecuredPreferencesHelper {

    private final SharedPreferences mPrefs;

    SecuredPreferencesHelper(Context context, String prefsName) {
        mPrefs = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE);
    }

    private String encrypt(String input) {
        return Base64.encodeToString(input.getBytes(), Base64.DEFAULT);
    }

    private String decrypt(String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }

    void put(String key, String value) {
        key = encrypt(key);
        value = encrypt(value);
        mPrefs.edit().putString(key, value).apply();
    }

    String get(String key, String defaultValue) {
        key = encrypt(key);
        defaultValue = encrypt(defaultValue);
        return decrypt(mPrefs.getString(key, defaultValue));
    }

    void deleteSavedData(String key) {
        key = encrypt(key);
        mPrefs.edit().remove(key).apply();
    }

    void clearAllData() {
        mPrefs.edit().clear().apply();
    }
}
