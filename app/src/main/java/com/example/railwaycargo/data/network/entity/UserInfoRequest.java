package com.example.railwaycargo.data.network.entity;

public class UserInfoRequest {
    public String inn;
    public String singKey;

    @Override
    public String toString() {
        return "{" +
                '\"'+"inn"+'\"'+":" + '\"'+ inn + '\"' +","+
                '\"'+"singKey"+'\"'+":" + '\"'+ singKey + '\"' +
                '}';
    }
}
