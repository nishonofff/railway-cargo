package com.example.railwaycargo.data.network.entity;

import com.squareup.moshi.Json;

import io.reactivex.annotations.Nullable;

public class DetailsResponse extends BaseResponse {

    @Json(name = "name")
    public String name;
    @Json(name = "inn")
    public String stir;
    @Json(name = "contract")
    public String contract;
    @Json(name = "contractNumber")
    public String contractNumber;
    @Json(name = "contractDate")
    public String contractDate;
    @Nullable
    @Json(name = "lastPaymentDate")
    public String lastPaymentDate;
    @Nullable
    @Json(name = "lastPaymentAmount")
    public String lastPaymentAmount;
    @Json(name = "balance")
    public String balance;
    @Json(name = "credit")
    public String credit;
    @Json(name = "debit")
    public String debit;
    @Json(name = "lastUpdatedDate")
    public String lastUpdatedDate;

}
