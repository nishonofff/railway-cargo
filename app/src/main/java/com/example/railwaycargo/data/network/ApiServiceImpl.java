package com.example.railwaycargo.data.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.railwaycargo.data.network.entity.GClassifiersResponse;
import com.example.railwaycargo.data.network.entity.OrdersResponse;
import com.example.railwaycargo.data.network.entity.StationsResponse;
import com.example.railwaycargo.data.network.entity.UserResponse;
import com.example.railwaycargo.utils.AppConstants;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static io.reactivex.schedulers.Schedulers.newThread;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public class ApiServiceImpl implements ApiService, AppConstants {

    private final Context mContext;
    private final API mAPI;
    private String mLocale;

    public ApiServiceImpl(Context mContext, API API) {
        this.mContext = mContext;

        this.mAPI = API;
    }

    @Override
    public void setLocale(String locale) {
        mLocale = locale;
    }

    @Override
    public boolean noConnection() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm != null ? cm.getActiveNetworkInfo() : null;
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return !activeNetwork.isConnected();
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return !activeNetwork.isConnected();
        }
        return true;
    }

    @Override
    public Single<UserResponse> onLogin(String phone) {
        return mAPI.onLogin(phone)
                .subscribeOn(newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<UserResponse> onLoginConfirm(String code) {
        return mAPI.onLoginConfirm(code)
                .subscribeOn(newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<OrdersResponse.Order> onPostOrder(String phone, Map<String, Object> body) {
        return mAPI.onPostOrder(phone,body)
                .subscribeOn(newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<List<StationsResponse.Station>> getStations() {
        return mAPI.getStations()
                .subscribeOn(newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<List<GClassifiersResponse.GClassifier>> getGroupClassifiers() {
        return mAPI.getGroupClassifiers()
                .subscribeOn(newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<List<OrdersResponse.Order>> getGOrders(String phone) {
        return mAPI.getOrders(phone)
                .subscribeOn(newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }


//    @Override
//    public Single<DetailsResponse> getInfo(RequestBody body) {
//        return mUpayAPI.getInfo(body)
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread());
//    }
//
//    @Override
//    public Single<UserResponse> getUserInfo(UserInfoRequest body) {
//        return mUpayAPI.getUserInfo(body)
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread());
//    }

}
