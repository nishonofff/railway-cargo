package com.example.railwaycargo.data.network.entity;

import com.squareup.moshi.Json;

public class UserResponse extends BaseResponse {

    @Json(name = "name")
    public String name;
    @Json(name = "inn")
    public String stir;
}
