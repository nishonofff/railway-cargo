package com.example.railwaycargo.data.network;

import com.example.railwaycargo.data.network.entity.GClassifiersResponse;
import com.example.railwaycargo.data.network.entity.OrdersResponse;
import com.example.railwaycargo.data.network.entity.StationsResponse;
import com.example.railwaycargo.data.network.entity.UserResponse;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public interface API {

//    @Headers("Content-Type: application/json")
//    @POST("find")
//    Single<DetailsResponse> getInfo(@Body RequestBody body);
//
//    @Headers("Content-Type: application/json")
//    @POST("findName")
//    Single<UserResponse> getUserInfo(@Body UserInfoRequest body);

    @GET("otp")
    Single<UserResponse> onLogin(@Query("phone") String phone);

    @POST("otp")
    Single<UserResponse> onLoginConfirm(@Query("code") String code);

    @GET("station")
    Single<List<StationsResponse.Station>> getStations();

    @GET("cargo/group/classifier")
    Single<List<GClassifiersResponse.GClassifier>> getGroupClassifiers();

    @GET("order")
    Single<List<OrdersResponse.Order>> getOrders(@Query("phone") String phone);

    @Headers("Content-Type: application/json")
    @POST("order")
    Single<OrdersResponse.Order> onPostOrder(@Query("phone") String phone,@Body Map<String, Object> body);

}
