package com.example.railwaycargo.data.network.entity;

import com.squareup.moshi.Json;

public class OrdersResponse extends BaseResponse {


    public static class Order {
        public int id;
        public String createdDate;
        public int distance;
        public String endDate;
        public int from;
        public int group;
        public boolean perishable;
        public int price;
        public String startDate;
        public String status;
        public int to;
        public String updatedDate;
        public int wagonQuantity;
        public boolean wagonRent;
        public int weight;
    }

}
