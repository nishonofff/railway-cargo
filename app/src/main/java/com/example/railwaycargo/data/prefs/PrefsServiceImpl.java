package com.example.railwaycargo.data.prefs;

import android.content.Context;

import com.example.railwaycargo.utils.AppConstants;


/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public class PrefsServiceImpl implements PrefsService, AppConstants {

    private final SecuredPreferencesHelper mSecuredPrefs;

    public PrefsServiceImpl(Context context, String prefsFileName) {
        mSecuredPrefs = new SecuredPreferencesHelper(context, prefsFileName);
    }

    @Override
    public void clearPrefs() {
        mSecuredPrefs.clearAllData();
    }

    @Override
    public String getLocale() {
        return mSecuredPrefs.get(LOCALE, "ru");
    }

    @Override
    public void setLocale(String locale) {
        mSecuredPrefs.put(LOCALE, locale);
    }

    @Override
    public String getUserTin(String key) {
        return mSecuredPrefs.get(key, "0");
    }

    @Override
    public String getPhoneNumber(String key) {
        return mSecuredPrefs.get(key, "0");
    }

    @Override
    public void setUserTin(String key, String tin) {
        mSecuredPrefs.put(key, tin);
    }

    @Override
    public void setPhoneNumber(String key, String phoneNumber) {
        mSecuredPrefs.put(key, phoneNumber);
    }

    @Override
    public void clearUserData() {
        mSecuredPrefs.clearAllData();
    }

    @Override
    public boolean loggedIn() {
        String result = mSecuredPrefs.get("logged_in", "0");
        return !result.equals("0");
    }

    @Override
    public void setUserStatus() {
        mSecuredPrefs.put("logged_in", "1");
    }
}
