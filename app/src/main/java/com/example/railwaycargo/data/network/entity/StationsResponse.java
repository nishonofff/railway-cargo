package com.example.railwaycargo.data.network.entity;

import com.squareup.moshi.Json;

import java.util.List;

public class StationsResponse extends BaseResponse {
    public static class Station {
        @Json(name = "id")
        public int id;
        @Json(name = "name")
        public String name;
        public String code;
    }
}
