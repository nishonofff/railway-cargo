package com.example.railwaycargo.data.network;

import com.example.railwaycargo.data.network.entity.GClassifiersResponse;
import com.example.railwaycargo.data.network.entity.OrdersResponse;
import com.example.railwaycargo.data.network.entity.StationsResponse;
import com.example.railwaycargo.data.network.entity.UserResponse;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public interface ApiService {
    ///if app language will be needed
    void setLocale(String locale);

    /**
     * Checking internet connection
     *
     * @return true if there is not internet connection, otherwise false
     */
    boolean noConnection();

    Single<UserResponse> onLogin(String phone);

    Single<UserResponse> onLoginConfirm(String code);

    Single<OrdersResponse.Order> onPostOrder(String phone, Map<String, Object> body);

    Single<List<StationsResponse.Station>> getStations();

    Single<List<GClassifiersResponse.GClassifier>> getGroupClassifiers();

    Single<List<OrdersResponse.Order>> getGOrders(String phone);

}
