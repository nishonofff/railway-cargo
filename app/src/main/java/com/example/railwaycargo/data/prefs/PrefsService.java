package com.example.railwaycargo.data.prefs;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public interface PrefsService {

    String LOCALE = "locale";

    void clearPrefs();

    String getLocale();

    void setLocale(String locale);

    String getUserTin(String key);

    String getPhoneNumber(String key);

    void setUserTin(String key, String tin);

    void setPhoneNumber(String key, String phoneNumber);

    void clearUserData();

    boolean loggedIn();

    void setUserStatus();

}
