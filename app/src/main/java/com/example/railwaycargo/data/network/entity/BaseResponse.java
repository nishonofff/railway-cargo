package com.example.railwaycargo.data.network.entity;

import com.squareup.moshi.Json;

public class BaseResponse {
    @Json(name = "status")
    public int status;
    @Json(name = "message")
    public String message;
}
