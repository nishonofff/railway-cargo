package com.example.railwaycargo.data.network.entity;

import com.squareup.moshi.Json;

public class GClassifiersResponse extends BaseResponse {
    public static class GClassifier {
        public int code;
        @Json(name = "name")
        public String name;
    }
}
