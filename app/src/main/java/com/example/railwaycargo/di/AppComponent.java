package com.example.railwaycargo.di;

import com.example.railwaycargo.App;
import com.example.railwaycargo.di.module.AppModule;
import com.example.railwaycargo.ui.auth.AuthPresenter;
import com.example.railwaycargo.ui.main.home_fragment.HomePresenter;
import com.example.railwaycargo.ui.main.order_fragment.OrderPresenter;
import com.example.railwaycargo.ui.settings.SettingsPresenter;
import com.example.railwaycargo.ui.splash_screen.SplashScreenPresenter;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(App app);

    void inject(AuthPresenter presenter);

    void inject(OrderPresenter presenter);

    void inject(HomePresenter presenter);

    void inject(SplashScreenPresenter presenter);

    void inject(SettingsPresenter presenter);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder app(App app);

        AppComponent build();
    }
}
