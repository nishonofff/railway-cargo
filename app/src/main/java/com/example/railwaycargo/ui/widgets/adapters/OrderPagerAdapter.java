package com.example.railwaycargo.ui.widgets.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.railwaycargo.ui.history.active.ActiveCarriagesFragment;
import com.example.railwaycargo.ui.orders.accepted.AcceptedOrdersFragment;
import com.example.railwaycargo.ui.orders.rejected.RejectedOrdersFragment;
import com.example.railwaycargo.ui.orders.send.SendOrdersFragment;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public class OrderPagerAdapter extends FragmentPagerAdapter {

    private String[] mPageTitles;

    public OrderPagerAdapter(FragmentManager fragmentManager, String[] pageTitles) {
        super(fragmentManager);
        mPageTitles = pageTitles;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mPageTitles[position];
    }

    @Override
    public int getCount() {
        return mPageTitles.length;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return SendOrdersFragment.newInstance();
            case 1:
                return AcceptedOrdersFragment.newInstance();
            case 2:
                return RejectedOrdersFragment.newInstance();
            default:
                return ActiveCarriagesFragment.newInstance();
        }
    }
}
