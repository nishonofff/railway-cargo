package com.example.railwaycargo.ui.main.calculator_fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.railwaycargo.R;
import com.example.railwaycargo.ui.main.calculator_fragment.child.CalculatorResultActivity;
import com.example.railwaycargo.ui.main.callback.MainActivityListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class CalculatorFragment extends Fragment {

    @BindView(R.id.scroll_view)
    View mCalculatorView;

    private MainActivityListener mListener;

    private FragmentManager mFragmentManager;

    public static CalculatorFragment newInstance() {
        CalculatorFragment fragment = new CalculatorFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (MainActivityListener) context;
        mListener.changeTitleText("Калькулятор");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentManager = getChildFragmentManager();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_calculator, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.calculate_button)
    void onCalculateClicked() {
        Intent intent = new Intent(getActivity(), CalculatorResultActivity.class);
        startActivity(intent);
    }

}
