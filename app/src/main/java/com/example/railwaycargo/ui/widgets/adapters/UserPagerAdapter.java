package com.example.railwaycargo.ui.widgets.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.railwaycargo.ui.user_account.balance.BalanceFragment;
import com.example.railwaycargo.ui.user_account.info.UserInfoFragment;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public class UserPagerAdapter extends FragmentPagerAdapter {

    private String[] mPageTitles;


    public UserPagerAdapter(FragmentManager fragmentManager, String[] pageTitles) {
        super(fragmentManager);
        mPageTitles = pageTitles;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mPageTitles[position];
    }

    @Override
    public int getCount() {
        return mPageTitles.length;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return UserInfoFragment.newInstance();
            case 1:
                return BalanceFragment.newInstance();
            default:
                return UserInfoFragment.newInstance();
        }
    }
}
