package com.example.railwaycargo.ui.auth.login_fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.railwaycargo.R;
import com.example.railwaycargo.ui.auth.callback.AuthActivityListener;
import com.example.railwaycargo.ui.auth.callback.LoginFragmentCallback;
import com.example.railwaycargo.ui.auth.sign_up_activity.SignUpActivity;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public class LoginFragment extends Fragment implements LoginFragmentCallback {

    @BindView(R.id.login_email_edit_text)
    EditText mLoginEmail;
    @BindView(R.id.login_password_edit_text)
    EditText mLoginPassword;
    @BindView(R.id.sign_up)
    TextView mSignUp;
    @BindView(R.id.log_in)
    TextView mLogIn;
    @BindView(R.id.sign_up_view)
    View mSignUpView;
    @BindView(R.id.sign_up_select_view)
    View mSignUpSelectView;
    @BindView(R.id.log_in_view)
    View mLogInView;
    @BindColor(R.color.colorPrimaryText)
    int mPrimaryText;
    @BindColor(R.color.colorSecondaryText)
    int mSecondaryText;


    public static final String TAG = LoginFragment.class.getName();

    private AuthActivityListener mActivityListener;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivityListener = (AuthActivityListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }


    @OnClick(R.id.log_in)
    void onLogin() {
        mLogIn.setTextColor(mPrimaryText);
        mSignUp.setTextColor(mSecondaryText);
        mSignUpSelectView.setVisibility(View.GONE);
        mLogInView.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.sign_up)
    void onSignUp() {
        mLogIn.setTextColor(mSecondaryText);
        mSignUp.setTextColor(mPrimaryText);
        mSignUpSelectView.setVisibility(View.VISIBLE);
        mLogInView.setVisibility(View.GONE);
    }

    @OnClick(R.id.individual_user)
    void onIndividualUserSelected() {
        mSignUpSelectView.setVisibility(View.GONE);
        mSignUpView.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.legal_entity)
    void onLegalEntitySelected() {
        startActivity(new Intent(getActivity(), SignUpActivity.class));
    }

    @OnClick(R.id.log_in_button)
    void onLoginClicked() {

        String email = mLoginEmail.getText().toString();
        if (email.isEmpty()) {
            mLoginEmail.setError(getString(R.string.empty_msg_email));
            return;
        }
        String password = mLoginPassword.getText().toString();
        if (password.isEmpty()) {
            mLoginPassword.setError(getString(R.string.empty_msg_email));
            return;
        }
        mActivityListener.onLogin(this, email, password);
    }


    @Override
    public void showLoginProgress() {

    }

    @Override
    public void hideLoginProgress() {

    }

    @Override
    public void onErrorEmail(String message) {
    }

    @Override
    public void onErrorPhoneNumber(String message) {
    }

}
