package com.example.railwaycargo.ui.auth.callback;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public interface AuthActivityListener {

    void setLoginVisibility(boolean visibility);

    void setSyncVisibility(boolean visibility);

    void onForgotPassword();

    void onLogin(LoginFragmentCallback loginCallback, String email, String password);

    void onSignUp(LoginFragmentCallback loginCallback, String email, String password);

    void onConfirmation(ConfirmFragmentCallback confirmCallback, String confirmCode);

    void onRestorePassword(RestoreFragmentCallback restoreCallback, String phoneNumber);

    void onSetupPinCode(String pinCode);

    void onSyncUserInfo(SyncFragmentCallback syncCallback);

    void onMain();
}
