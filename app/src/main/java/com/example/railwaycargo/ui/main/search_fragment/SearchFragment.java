package com.example.railwaycargo.ui.main.search_fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.railwaycargo.R;
import com.example.railwaycargo.ui.main.callback.MainActivityListener;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnFocusChange;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class SearchFragment extends Fragment {

    @BindView(R.id.start_date_edit_text)
    EditText mStartDate;
    @BindView(R.id.end_date_edit_text)
    EditText mEndDate;

    private MainActivityListener mListener;
    DatePickerDialog picker;

    DatePickerDialog.OnDateSetListener mDateSetListener;

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (MainActivityListener) context;
        mListener.changeTitleText("Поиск");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        setUpDatePickerDialog();
    }

    @OnFocusChange(R.id.start_date_edit_text)
    void onStartDateEdited(boolean focused) {
        if (focused) {
            mDateSetListener = (view, year, monthOfYear, dayOfMonth) -> mStartDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            picker.show();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            picker.setOnDateSetListener(mDateSetListener);
        }
    }

    @OnFocusChange(R.id.end_date_edit_text)
    void onEndDateEdited(boolean focused) {
        if (focused) {
            mDateSetListener = (view, year, monthOfYear, dayOfMonth) -> mEndDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
            picker.show();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            picker.setOnDateSetListener(mDateSetListener);
        }
    }

    void setUpDatePickerDialog() {
        final Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        picker = new DatePickerDialog(getActivity()
                , android.R.style.Theme_Holo_Light_Dialog_MinWidth
                , mDateSetListener
                , year, month, day);
        picker.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
}
