package com.example.railwaycargo.ui.main.order_fragment;

import com.arellomobile.mvp.MvpView;
import com.example.railwaycargo.data.network.entity.GClassifiersResponse;
import com.example.railwaycargo.data.network.entity.StationsResponse;

import java.util.List;

public interface OrderView extends MvpView {

    void showNoInternetConnection();

    void showProgress();

    void hideProgress();

    void postOrderSuccess();

    void postOrderError();

    void setStationsList(List<StationsResponse.Station> stationsList);

    void setGClassifiersList(List<GClassifiersResponse.GClassifier> gClassifierList);

    void loadError(String message);
}
