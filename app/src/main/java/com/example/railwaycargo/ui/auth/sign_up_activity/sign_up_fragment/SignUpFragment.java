package com.example.railwaycargo.ui.auth.sign_up_activity.sign_up_fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.railwaycargo.R;
import com.example.railwaycargo.ui.auth.callback.AuthActivityListener;
import com.example.railwaycargo.ui.auth.callback.LoginFragmentCallback;
import com.example.railwaycargo.ui.auth.sign_up_activity.SignUpActivity;
import com.example.railwaycargo.ui.main.MainActivity;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public class SignUpFragment extends Fragment {

    @BindView(R.id.view_1)
    View view1;
    @BindView(R.id.view_2)
    View view2;
    @BindView(R.id.view_3)
    View view3;

    @BindColor(R.color.colorPrimaryText)
    int mPrimaryText;
    @BindColor(R.color.colorSecondaryText)
    int mSecondaryText;

    private int mPosition;

    public static final String TAG = SignUpFragment.class.getName();

    private AuthActivityListener mActivityListener;

    public static SignUpFragment newInstance(int position) {
        SignUpFragment fragment = new SignUpFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mPosition = getArguments().getInt("position");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        switch (mPosition) {
            case 0:
                view1.setVisibility(View.VISIBLE);
                view2.setVisibility(View.GONE);
                view3.setVisibility(View.GONE);
                break;
            case 1:
                view1.setVisibility(View.GONE);
                view2.setVisibility(View.VISIBLE);
                view3.setVisibility(View.GONE);
                break;
            case 2:
                view1.setVisibility(View.GONE);
                view2.setVisibility(View.GONE);
                view3.setVisibility(View.VISIBLE);
                break;
            default:
                view1.setVisibility(View.VISIBLE);
                view2.setVisibility(View.GONE);
                view3.setVisibility(View.GONE);
                break;
        }
    }


    @OnClick(R.id.passport_upload)
    void onPassportUpload() {
        getFile();
    }

    @OnClick(R.id.document_upload)
    void onDocumentUpload() {
        getFile();
    }

    @OnClick(R.id.doc_upload)
    void onDocUpload() {
        getFile();
    }

    void getFile() {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select PDF"), 1);
    }

    @OnClick(R.id.log_in_button)
    void onLoginClicked() {
        startActivity(new Intent(getActivity(), MainActivity.class));
    }
}
