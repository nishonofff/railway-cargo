package com.example.railwaycargo.ui.main.search_fragment.child;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.railwaycargo.R;
import com.example.railwaycargo.ui.main.callback.MainActivityListener;

import butterknife.ButterKnife;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class SearchChildFragment extends Fragment {

    private MainActivityListener mListener;

    public static SearchChildFragment newInstance( ) {
        SearchChildFragment fragment = new SearchChildFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (MainActivityListener) context;
        mListener.changeTitleText("Поиск");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

    }

}
