package com.example.railwaycargo.ui.widgets.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.example.railwaycargo.R;


public class AlertDialogs {

    public static void showNoConnectionMessage(Context context, DialogInterface.OnClickListener postiveListener,
                                               DialogInterface.OnClickListener negativeListener) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(false)
                .setMessage(R.string.check_internet_connection)
                .setPositiveButton(R.string.retry, postiveListener)
                .setNegativeButton(android.R.string.cancel, negativeListener)
                .show();
    }

    public static void showSuccessDialog(Context context) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(true)
                .setMessage("Ваш заказ успешно отправлен !")
                .show();
    }

    public static void showErrorDialog(Context context) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(true)
                .setMessage("Что-то пошло не так. Пожалуйста, проверьте ваше соединение и попробуйте снова")
                .show();
    }

    public static void showLoginRequiredMessage(Context context, DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(context, R.style.AlertDialog)
                .setCancelable(false)
                .setMessage(R.string.login_required)
                .setPositiveButton(R.string.login, listener)
                .setNegativeButton(android.R.string.cancel,null)
                .create()
                .show();
    }

}
