package com.example.railwaycargo.ui.auth.callback;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public interface LoginFragmentCallback {

    void showLoginProgress();

    void hideLoginProgress();

    void onErrorEmail(String message);

    void onErrorPhoneNumber(String message);
}
