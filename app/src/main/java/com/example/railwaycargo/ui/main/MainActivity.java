package com.example.railwaycargo.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.railwaycargo.R;
import com.example.railwaycargo.ui.auth.AuthActivity;
import com.example.railwaycargo.ui.history.HistoryActivity;
import com.example.railwaycargo.ui.main.calculator_fragment.CalculatorFragment;
import com.example.railwaycargo.ui.main.callback.MainActivityListener;
import com.example.railwaycargo.ui.main.home_fragment.HomeFragment;
import com.example.railwaycargo.ui.main.order_fragment.OrderFragment;
import com.example.railwaycargo.ui.main.search_fragment.SearchFragment;
import com.example.railwaycargo.ui.orders.OrdersActivity;
import com.example.railwaycargo.ui.settings.SettingsActivity;
import com.example.railwaycargo.ui.user_account.UserAccountActivity;
import com.example.railwaycargo.ui.widgets.dialogs.AlertDialogs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class MainActivity extends AppCompatActivity implements MainActivityListener {

    private MainActivity mContext;

    @BindView(R.id.bottom_navbar_search)
    ImageView mNavbarSearch;

    @BindView(R.id.bottom_navbar_home)
    ImageView mNavbarHome;

    @BindView(R.id.bottom_navbar_order)
    ImageView mNavbarOrder;

    @BindView(R.id.bottom_navbar_calculator)
    ImageView mNavbarCalculator;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.toolbar_title_view)
    TextView mToolbarTitleView;

    @BindView(R.id.action_more)
    ImageView mActionMore;

    @BindView(R.id.contact_details_view)
    View mDetailsView;

    private FragmentManager mFragmentManager;
    public static boolean isLoggedIn = false;

    public static void start(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mFragmentManager = getSupportFragmentManager();
        mContext = this;
        onOrderClick();
    }


    @OnClick(R.id.bottom_navbar_home)
    public void onHomeClick() {
        clearSelectionBottomIcons();
        mNavbarHome.setSelected(true);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, HomeFragment.newInstance())
//                .addToBackStack(NewsFragment.TAG)
                .commit();
    }

    @OnClick(R.id.bottom_navbar_search)
    public void onSearchClick() {
        clearSelectionBottomIcons();

        mNavbarSearch.setSelected(true);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, SearchFragment.newInstance())
//                .addToBackStack(NewsFragment.TAG)
                .commit();
    }

    @OnClick(R.id.bottom_navbar_calculator)
    public void onCalculatorClick() {
        clearSelectionBottomIcons();

        mNavbarCalculator.setSelected(true);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, CalculatorFragment.newInstance())
//                .addToBackStack(NewsFragment.TAG)
                .commit();
    }

    @OnClick(R.id.bottom_navbar_order)
    public void onOrderClick() {

        clearSelectionBottomIcons();

        mNavbarOrder.setSelected(true);
        mFragmentManager.beginTransaction()
                .replace(R.id.container, OrderFragment.newInstance())
//                .addToBackStack(NewsFragment.TAG)
                .commit();
//        if (isLoggedIn) {
//            clearSelectionBottomIcons();
//
//            mNavbarOrder.setSelected(true);
//            mFragmentManager.beginTransaction()
//                    .replace(R.id.container, OrderFragment.newInstance())
////                .addToBackStack(NewsFragment.TAG)
//                    .commit();
//        } else {
//            AlertDialogs.showLoginRequiredMessage(this, (dialogInterface, i) -> startActivity(new Intent(this, AuthActivity.class)));
//        }
    }

    public void clearSelectionBottomIcons() {
        mNavbarHome.setSelected(false);
        mNavbarSearch.setSelected(false);
        mNavbarCalculator.setSelected(false);
        mNavbarOrder.setSelected(false);
    }

    @OnClick(R.id.action_more)
    void moreButtonClicked() {
        if (mDetailsView.getVisibility() == View.VISIBLE) {
            mActionMore.setImageResource(R.drawable.ic_arrow_down);
            mDetailsView.setVisibility(View.GONE);
        } else {
            mActionMore.setImageResource(R.drawable.ic_arrow_up);
            mDetailsView.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.navbar_user_account)
    void onUserAccountClicked() {
        Intent intent = new Intent(this, UserAccountActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.navbar_settings)
    void onSettingsClicked() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.navbar_history)
    void onHistoryClicked() {
        Intent intent = new Intent(this, HistoryActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.navbar_orders)
    void onOrdersClicked() {
        Intent intent = new Intent(this, OrdersActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.toolbar_menu)
    void onToolbarMenuClicked() {
        mDrawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void changeTitleText(String title) {
        mToolbarTitleView.setText(title);
    }
}

