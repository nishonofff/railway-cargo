package com.example.railwaycargo.ui.settings;

import android.content.Intent;
import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.railwaycargo.R;
import com.example.railwaycargo.ui.auth.AuthActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends MvpAppCompatActivity implements SettingsView {

    @InjectPresenter
    SettingsPresenter mSettingsPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.toolbar_back_button)
    void onToolbarBackClicked() {
        onBackPressed();
    }

    @OnClick(R.id.log_out_button)
    void onLogOutClicked() {
        mSettingsPresenter.logOut();
        startActivity(new Intent(this, AuthActivity.class));
    }

    @Override
    public void showNoInternetConnection() {

    }

}
