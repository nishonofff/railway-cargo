package com.example.railwaycargo.ui.widgets.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.railwaycargo.R;
import com.example.railwaycargo.data.network.entity.GClassifiersResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GClassifierAdapter extends RecyclerView.Adapter<GClassifierAdapter.ViewHolder> {

    private Context mContext;

    private static GClassifierItemClickListener mItemClickListener;
    private List<GClassifiersResponse.GClassifier> mGClassifierList;

    public GClassifierAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setItemClickListener(GClassifierItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void setGClassifierList(List<GClassifiersResponse.GClassifier> gClassifierList) {
        mGClassifierList = gClassifierList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.station_item, parent, false), mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setStationItem(mGClassifierList.get(i));
    }

    @Override
    public int getItemCount() {
        if (mGClassifierList != null)
            return mGClassifierList.size();
        else return 0;
    }

    public interface GClassifierItemClickListener {
        void onGClassifierItemClick(int position, GClassifiersResponse.GClassifier classifierItem);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.station_name)
        TextView mStationName;

        GClassifierItemClickListener mItemClickListener;

        public ViewHolder(@NonNull View itemView, GClassifierItemClickListener itemClickListener) {
            super(itemView);
            mItemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
        }

        public void setStationItem(GClassifiersResponse.GClassifier classifierItem) {
            mStationName.setText(classifierItem.name);
        }

        @OnClick(R.id.station_item)
        void onGClassifierClicked() {
            mItemClickListener.onGClassifierItemClick(getAdapterPosition(), mGClassifierList.get(getAdapterPosition()));
        }

    }
}
