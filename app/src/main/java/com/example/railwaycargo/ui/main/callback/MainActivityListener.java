package com.example.railwaycargo.ui.main.callback;

public interface MainActivityListener {

    void changeTitleText(String title);

}
