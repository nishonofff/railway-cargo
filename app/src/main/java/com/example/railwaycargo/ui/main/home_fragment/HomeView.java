package com.example.railwaycargo.ui.main.home_fragment;

import com.arellomobile.mvp.MvpView;
import com.example.railwaycargo.data.network.entity.GClassifiersResponse;
import com.example.railwaycargo.data.network.entity.OrdersResponse;
import com.example.railwaycargo.data.network.entity.StationsResponse;

import java.util.List;

public interface HomeView extends MvpView {

    void showNoInternetConnection();

    void showProgress();

    void hideProgress();

    void setOrdersList(List<OrdersResponse.Order> ordersList);

    void setStationsList(List<StationsResponse.Station> stationsList);

    void loadError(String message);
}
