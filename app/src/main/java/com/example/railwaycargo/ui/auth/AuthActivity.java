package com.example.railwaycargo.ui.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.railwaycargo.R;
import com.example.railwaycargo.ui.auth.callback.AuthActivityListener;
import com.example.railwaycargo.ui.auth.callback.ConfirmFragmentCallback;
import com.example.railwaycargo.ui.auth.callback.LoginFragmentCallback;
import com.example.railwaycargo.ui.auth.callback.RestoreFragmentCallback;
import com.example.railwaycargo.ui.auth.callback.SyncFragmentCallback;
import com.example.railwaycargo.ui.auth.confirm_fragment.ConfirmFragment;
import com.example.railwaycargo.ui.auth.login_fragment.LoginFragment;
import com.example.railwaycargo.ui.main.MainActivity;
import com.example.railwaycargo.ui.splash_screen.SplashScreenActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public class AuthActivity extends MvpAppCompatActivity implements AuthActivityListener, AuthView {

    @BindView(R.id.phone_number)
    TextInputEditText mPhoneNumberEditText;
    @BindView(R.id.phone_number_edit_text_layout)
    TextInputLayout mPhoneNumberEditTextLayout;
    @BindView(R.id.container_view)
    View mContainerView;
    @BindView(R.id.progress_view)
    View mProgressView;
    @BindView(R.id.login_button)
    Button mLoginButton;

    private FragmentManager mFragmentManager;

    @InjectPresenter
    AuthPresenter mAuthPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        ButterKnife.bind(this);

        mFragmentManager = getSupportFragmentManager();
//
//        mFragmentManager.beginTransaction()
//                .replace(R.id.container, LoginFragment.newInstance())
//                .addToBackStack(LoginFragment.TAG)
//                .commit();

        mPhoneNumberEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                if (mPhoneNumberEditText.getText().toString().length() < 5) {
                    mPhoneNumberEditText.setText("+998");
                }
            } else {
                if (mPhoneNumberEditText.getText().toString().length() < 5) {
                    mPhoneNumberEditText.setText("");
                }
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mPhoneNumberEditText.getWindowToken(), 0);
            }
        });

    }

    @OnClick(R.id.login_button)
    void onLoginClicked() {
        String phoneNumber = mPhoneNumberEditText.getText().toString();

        if (phoneNumber.length() < 13) {
            mPhoneNumberEditTextLayout.setError("Phone number must contain 14 characters");
            return;
        }
        mAuthPresenter.onLogin(phoneNumber);
    }

    @Override
    public void setLoginVisibility(boolean visibility) {

    }

    @Override
    public void setSyncVisibility(boolean visibility) {

    }

    @Override
    public void onForgotPassword() {

    }

    @OnClick(R.id.container_view)
    void onContainerClicked() {
        mContainerView.requestFocus();
    }

    @Override
    public void onLogin(LoginFragmentCallback loginCallback, String email, String password) {

        mFragmentManager.beginTransaction()
                .replace(R.id.container, ConfirmFragment.newInstance())
                .addToBackStack(LoginFragment.TAG)
                .commit();
    }

    @Override
    public void onSignUp(LoginFragmentCallback loginCallback, String email, String password) {

        mFragmentManager.beginTransaction()
                .replace(R.id.container, ConfirmFragment.newInstance())
                .addToBackStack(LoginFragment.TAG)
                .commit();
    }

    @Override
    public void onConfirmation(ConfirmFragmentCallback confirmCallback, String confirmCode) {
        mAuthPresenter.onConfirm(confirmCode);
        MainActivity.isLoggedIn = true;
    }

    @Override
    public void onRestorePassword(RestoreFragmentCallback restoreCallback, String phoneNumber) {

    }

    @Override
    public void onSetupPinCode(String pinCode) {

    }

    @Override
    public void onSyncUserInfo(SyncFragmentCallback syncCallback) {

    }

    @Override
    public void onMain() {

    }

    @Override
    public void showNoInternetConnection() {

    }

    @Override
    public void showLoginProgress() {
        mProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginProgress() {
        mProgressView.setVisibility(View.GONE);
    }

    @Override
    public void loginSuccess(String message) {
        mFragmentManager.beginTransaction()
                .replace(R.id.container, ConfirmFragment.newInstance())
                .addToBackStack(LoginFragment.TAG)
                .commit();

        mLoginButton.setVisibility(View.GONE);
    }

    @Override
    public void userNotFoundError(String message) {

    }

    @Override
    public void incorrectPasswordError(String message) {

    }

    @Override
    public void unexpectedError(String message) {

    }

    @Override
    public void showConfirmProgress() {
        mProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideConfirmProgress() {
        mProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void confirmSuccess(String message) {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void confirmCodeError(String message) {

    }

    @Override
    public void showRestoreProgress() {

    }

    @Override
    public void hideRestoreProgress() {

    }

    @Override
    public void restorePasswordSuccess(String message) {

    }

    @Override
    public void authorizationNotFoundError(String message) {

    }

    @Override
    public void syncUserInfo(String username) {

    }

    @Override
    public void showSyncProgress() {

    }

    @Override
    public void hideSyncProgress() {

    }

    @Override
    public void syncSuccess(String message) {

    }


//    public void setupCardNumberField() {
//        mPhoneNumberEditText.addTextChangedListener(new TextInputLayoutTextWatcher(mCardNumberLayout, this)
//                .setTextChangeListener(text -> mPresenter.onChangeCardNumber(text)));
//        mPhoneNumberEditText.addTextChangedListener(new MaskTextWatcher
//                .Builder(mPhoneNumberEditText, "#### #### #### ####", -1)
//                .setWrongFormatListener(position ->
//                        onSetInputFieldError(mCardNumberLayout, R.string.error_message_card_number_format))
//                .setTextReadyListener((position, serviceType, value) -> {
//                    mNextButton.setEnabled(true);
//                    onNextClicked(mNextButton);
//                })
//                .build());
//        mPhoneNumberEditText.setOnFocusChangeListener(new TextInputFocusChangeListener(mContext, R.string.placeholder_card_number));
//    }


}
