package com.example.railwaycargo.ui.main.order_fragment;

import android.annotation.SuppressLint;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.railwaycargo.App;
import com.example.railwaycargo.data.network.ApiService;
import com.example.railwaycargo.data.network.entity.GClassifiersResponse;
import com.example.railwaycargo.data.network.entity.OrdersResponse;
import com.example.railwaycargo.data.network.entity.StationsResponse;
import com.example.railwaycargo.data.prefs.PrefsService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

@InjectViewState
public class OrderPresenter extends MvpPresenter<OrderView> {

    private static final String TAG = OrderPresenter.class.getName();

    @Inject
    ApiService mApiService;

    @Inject
    PrefsService mPrefsService;

    private String mPhoneNumber;

    OrderPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void getStations() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            Log.w(TAG, "No internet connection");
            return;
        }
        getViewState().showProgress();

        mApiService.getStations().subscribe((List<StationsResponse.Station> response) -> {
            getViewState().hideProgress();
            getViewState().setStationsList(response);

        }, (Throwable throwable) -> {
            getViewState().hideProgress();
        });

    }

    @SuppressLint("CheckResult")
    void getGClassifiers() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            Log.w(TAG, "No internet connection");
            return;
        }
        getViewState().showProgress();

        mApiService.getGroupClassifiers().subscribe((List<GClassifiersResponse.GClassifier> response) -> {
            getViewState().hideProgress();
            getViewState().setGClassifiersList(response);

        }, (Throwable throwable) -> {
            getViewState().hideProgress();
        });

    }

    @SuppressLint("CheckResult")
    void postOrder(String startDate,
                   String endDate,
                   int startStationId,
                   int endStationId,
                   int groupClassifier,
                   boolean perishable,
                   boolean trainRent,
                   int quantity,
                   int weight) {

        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            Log.w(TAG, "No internet connection");
            return;
        }
        getViewState().showProgress();

        mPhoneNumber = mPrefsService.getPhoneNumber("phone");

        Map<String, Object> body = new HashMap<>();
        body.put("from", startStationId);
        body.put("to", endStationId);
        body.put("group", groupClassifier);
        body.put("perishable", perishable);
        body.put("wagonRent", trainRent);
        body.put("weight", weight);
        body.put("wagonQuantity", quantity);
        body.put("startDate", startDate);
        body.put("endDate", endDate);
        body.put("status", "NEW");

        mApiService.onPostOrder(mPhoneNumber, body).subscribe((OrdersResponse.Order response) -> {
            getViewState().hideProgress();
            getViewState().postOrderSuccess();

        }, (Throwable throwable) -> {
            getViewState().postOrderError();
            getViewState().hideProgress();
        });

    }

}
