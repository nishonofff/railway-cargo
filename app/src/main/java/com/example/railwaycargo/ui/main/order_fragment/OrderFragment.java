package com.example.railwaycargo.ui.main.order_fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.railwaycargo.R;
import com.example.railwaycargo.data.network.entity.GClassifiersResponse;
import com.example.railwaycargo.data.network.entity.StationsResponse;
import com.example.railwaycargo.ui.main.callback.MainActivityListener;
import com.example.railwaycargo.ui.widgets.adapters.GClassifierAdapter;
import com.example.railwaycargo.ui.widgets.adapters.StationsAdapter;
import com.example.railwaycargo.ui.widgets.dialogs.AlertDialogs;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class OrderFragment extends MvpAppCompatFragment
        implements StationsAdapter.StationItemClickListener, OrderView, GClassifierAdapter.GClassifierItemClickListener {

    @BindView(R.id.starting_station)
    EditText mStartingStation;
    @BindView(R.id.destination_station)
    EditText mDestinationStation;
    @BindView(R.id.group_classifier)
    EditText mGClassifier;
    @BindView(R.id.train_perishable_switch)
    Switch mTrainPerishableSwitch;
    @BindView(R.id.train_rent_switch)
    Switch mTrainRentSwitch;
    @BindView(R.id.start_date_edit_text)
    EditText mStartDateView;
    @BindView(R.id.end_date_edit_text)
    EditText mEndDateView;
    @BindView(R.id.train_quantity)
    EditText mTrainQuantityView;
    @BindView(R.id.train_weight)
    EditText mTrainWeightView;

    @BindView(R.id.progress_view)
    View mProgressView;

    @InjectPresenter
    OrderPresenter mOrderPresenter;

    private MainActivityListener mListener;

    private AlertDialog mAlertDialog;
    private AlertDialog mGCAlertDialog;
    private RecyclerView mRecyclerView;
    private RecyclerView mGCRecyclerView;
    private StationsAdapter mStationsAdapter;
    private GClassifierAdapter mGClassifierAdapter;
    private List<StationsResponse.Station> mStationsList;
    private String mStationType;
    private int mStartStationID, mEndStationID, mGroupClassifier;

    private DatePickerDialog mDatePicker;
    private DatePickerDialog.OnDateSetListener mDateSetListener;

    private String mStartDate, mEndDate;

    public static OrderFragment newInstance() {
        OrderFragment fragment = new OrderFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (MainActivityListener) context;
        mListener.changeTitleText("Сделать заказ");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mOrderPresenter.getStations();
        mOrderPresenter.getGClassifiers();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_order, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        setUpSelectionDialog();

        setUpDatePickerDialog();

        setUpGCSelectionDialog();
    }

    @OnClick(R.id.starting_station)
    void onSelectStartingStation() {
        mAlertDialog.setTitle("Выберите начальную станцию");
        mStationType = "start";
        mAlertDialog.show();
    }

    @OnClick(R.id.destination_station)
    void onSelectDestinationStation() {
        mAlertDialog.setTitle("Выберите пункт назначения");
        mStationType = "destination";
        mAlertDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @OnClick(R.id.group_classifier)
    void onSelectGroupClassifier() {
        mGCAlertDialog.setTitle("Выберите групповой классификатор");
        mGCAlertDialog.show();
    }

    @OnClick(R.id.start_date_edit_text)
    void onStartDateClicked() {
        mDateSetListener = (view, year, month, day) -> {
            String newDay = String.valueOf(day);
            if (day < 10) {
                newDay = "0" + day;
            }
            String newMonth = String.valueOf(month);
            if (month < 10) {
                newMonth = "0" + month;
            }

            mStartDate = year + "-" + (newMonth) + "-" + newDay;

            mStartDateView.setText(day + "/" + (month + 1) + "/" + year);
            mStartDateView.setError(null);
        };

        mDatePicker.show();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mDatePicker.setOnDateSetListener(mDateSetListener);
        }
    }

    @OnClick(R.id.end_date_edit_text)
    void onEndDateClicked() {
        mDateSetListener = (view, year, month, day) -> {
            String newDay = String.valueOf(day);
            if (day < 10) {
                newDay = "0" + day;
            }
            String newMonth = String.valueOf(month);
            if (month < 10) {
                newMonth = "0" + month;
            }

            mEndDate = year + "-" + (newMonth) + "-" + newDay;

            mEndDateView.setText(day + "/" + (month + 1) + "/" + year);
            mEndDateView.setError(null);
        };

        mDatePicker.show();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mDatePicker.setOnDateSetListener(mDateSetListener);
        }
    }

    @Override
    public void showNoInternetConnection() {

    }

    @Override
    public void setStationsList(List<StationsResponse.Station> stationsList) {
        mStationsList = stationsList;
        mStationsAdapter.setStationsList(mStationsList);
    }

    @Override
    public void setGClassifiersList(List<GClassifiersResponse.GClassifier> gClassifierList) {
        mGClassifierAdapter.setGClassifierList(gClassifierList);
    }

    @Override
    public void postOrderSuccess() {
        AlertDialogs.showSuccessDialog(getActivity());
    }

    @Override
    public void postOrderError() {
        AlertDialogs.showErrorDialog(getActivity());
    }

    @Override
    public void showProgress() {
        mProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressView.setVisibility(View.GONE);
    }

    @Override
    public void loadError(String message) {

    }

    @OnClick(R.id.order_button)
    void onOrderClicked() {
        int trainQuantity = 0, trainWeight = 0;
        boolean perishable = mTrainPerishableSwitch.isChecked();
        boolean trainRent = mTrainRentSwitch.isChecked();
        if (!mTrainQuantityView.getText().toString().isEmpty()) {
            trainQuantity = Integer.parseInt(mTrainQuantityView.getText().toString());
        }

        if (!mTrainWeightView.getText().toString().isEmpty()) {
            trainWeight = Integer.parseInt(mTrainWeightView.getText().toString());
        }

        if (mStartDateView.getText().toString().isEmpty()) {
            mStartDateView.setError("Start date should be selected");

            return;
        }
        if (mEndDateView.getText().toString().isEmpty()) {
            mEndDateView.setError("End date should be selected");
            return;
        }
        if (trainQuantity < 1) {
            mTrainQuantityView.setError("Quantity can not be less than 1");
            return;
        }
        if (trainQuantity > 54) {
            mTrainQuantityView.setError("Quantity can not be more than 54");
            return;
        }
        if (trainWeight < 10) {
            mTrainWeightView.setError("Weight must not be less than 10");
            return;
        }

        mOrderPresenter.postOrder(mStartDate, mEndDate, mStartStationID, mEndStationID,
                mGroupClassifier, perishable, trainRent, trainQuantity, trainWeight);
    }

    void setUpDatePickerDialog() {
        final Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        mDatePicker = new DatePickerDialog(getActivity()
                , android.R.style.Theme_Holo_Light_Dialog_MinWidth
                , mDateSetListener
                , year, month, day);
        mDatePicker.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void setUpSelectionDialog() {
        mRecyclerView = new RecyclerView(getActivity());
        mAlertDialog = new AlertDialog.Builder(getActivity())
                .setTitle("Select station")
                .setView(mRecyclerView).create();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mStationsAdapter = new StationsAdapter(getActivity());
        mStationsAdapter.setItemClickListener(this);
        mRecyclerView.setAdapter(mStationsAdapter);
    }

    private void setUpGCSelectionDialog() {
        mGCRecyclerView = new RecyclerView(getActivity());
        mGCAlertDialog = new AlertDialog.Builder(getActivity())
                .setTitle("Select group classifier")
                .setView(mGCRecyclerView).create();

        mGCRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mGClassifierAdapter = new GClassifierAdapter(getActivity());
        mGClassifierAdapter.setItemClickListener(this);
        mGCRecyclerView.setAdapter(mGClassifierAdapter);
    }


    @Override
    public void onStationItemClick(int position, StationsResponse.Station stationItem) {
        if (mStationType.equals("start")) {
            mStartStationID = stationItem.id;
            mStartingStation.setText(stationItem.name);
        } else {
            mEndStationID = stationItem.id;
            mDestinationStation.setText(stationItem.name);
        }
        mAlertDialog.dismiss();

    }

    @Override
    public void onGClassifierItemClick(int position, GClassifiersResponse.GClassifier classifierItem) {
        mGClassifier.setText(classifierItem.name);
        mGCAlertDialog.dismiss();
        mGroupClassifier = classifierItem.code;
    }
}
