package com.example.railwaycargo.ui.auth.confirm_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.railwaycargo.R;
import com.example.railwaycargo.ui.auth.callback.AuthActivityListener;
import com.example.railwaycargo.ui.auth.callback.ConfirmFragmentCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public class ConfirmFragment extends Fragment implements ConfirmFragmentCallback {

    @BindView(R.id.confirm_code_edit_text)
    EditText mConfirmCode;

    public static final String TAG = ConfirmFragment.class.getName();


    private AuthActivityListener mActivityListener;

    public static ConfirmFragment newInstance() {
        ConfirmFragment fragment = new ConfirmFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivityListener = (AuthActivityListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_confirm, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @OnClick(R.id.confirm_button)
    void onConfirmClicked() {
        String confirmCode = mConfirmCode.getText().toString();
        if (confirmCode.isEmpty()) {
            mConfirmCode.setError(getString(R.string.empty_msg_password));
            return;
        }
        mActivityListener.onConfirmation(this, confirmCode);
    }

    @Override
    public void showConfirmProgress() {

    }

    @Override
    public void hideConfirmProgress() {

    }

    @Override
    public void onErrorConfirmCode(String message) {

    }
}
