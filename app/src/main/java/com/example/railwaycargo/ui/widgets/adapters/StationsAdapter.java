package com.example.railwaycargo.ui.widgets.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.railwaycargo.R;
import com.example.railwaycargo.data.network.entity.StationsResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StationsAdapter extends RecyclerView.Adapter<StationsAdapter.ViewHolder> {

    private Context mContext;

    private static StationItemClickListener mItemClickListener;
    private List<StationsResponse.Station> mStationsList;

    public StationsAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setItemClickListener(StationItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void setStationsList(List<StationsResponse.Station> stationsList) {
        mStationsList = stationsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.station_item, parent, false), mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setStationItem(mStationsList.get(i));
    }

    @Override
    public int getItemCount() {
        if (mStationsList != null)
            return mStationsList.size();
        else return 0;
    }

    public interface StationItemClickListener {
        void onStationItemClick(int position, StationsResponse.Station stationItem);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.station_name)
        TextView mStationName;

        StationItemClickListener mItemClickListener;

        public ViewHolder(@NonNull View itemView, StationItemClickListener itemClickListener) {
            super(itemView);
            mItemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
        }

        public void setStationItem(StationsResponse.Station stationItem) {
            mStationName.setText(stationItem.name);
        }

        @OnClick(R.id.station_item)
        void onStationClicked() {
            mItemClickListener.onStationItemClick(getAdapterPosition(), mStationsList.get(getAdapterPosition()));
        }

    }
}
