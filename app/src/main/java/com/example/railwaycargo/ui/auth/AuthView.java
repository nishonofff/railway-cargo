package com.example.railwaycargo.ui.auth;

import com.arellomobile.mvp.MvpView;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

interface AuthView extends MvpView {

    void showNoInternetConnection();

    void showLoginProgress();

    void hideLoginProgress();

    void loginSuccess(String message);

    void userNotFoundError(String message);

    void incorrectPasswordError(String message);

    void unexpectedError(String message);

    void showConfirmProgress();

    void hideConfirmProgress();

    void confirmSuccess(String message);

    void confirmCodeError(String message);

    void showRestoreProgress();

    void hideRestoreProgress();

    void restorePasswordSuccess(String message);

    void authorizationNotFoundError(String message);

    void syncUserInfo(String username);

    void showSyncProgress();

    void hideSyncProgress();

    void syncSuccess(String message);
}
