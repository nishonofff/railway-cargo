package com.example.railwaycargo.ui.auth.sign_up_activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.railwaycargo.R;
import com.example.railwaycargo.ui.widgets.adapters.SignUpPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignUpActivity extends AppCompatActivity {

    @BindView(R.id.pager)
    ViewPager mViewPager;
    @BindView(R.id.tabDots)
    TabLayout mTabLayout;


    private SignUpPagerAdapter mPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ButterKnife.bind(this);


        mPagerAdapter = new SignUpPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);

        mTabLayout.setupWithViewPager(mViewPager);
    }
}
