package com.example.railwaycargo.ui.main.calculator_fragment.child;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.railwaycargo.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class CalculatorResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator_result);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.toolbar_back_button)
    void onToolbarBackClicked() {
        onBackPressed();
    }
}
