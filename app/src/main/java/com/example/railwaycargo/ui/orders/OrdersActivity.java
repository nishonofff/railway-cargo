package com.example.railwaycargo.ui.orders;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.railwaycargo.R;
import com.example.railwaycargo.ui.widgets.adapters.HistoryPagerAdapter;
import com.example.railwaycargo.ui.widgets.adapters.OrderPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrdersActivity extends AppCompatActivity {

    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    private OrderPagerAdapter mPagerAdapter;

    String[] mPageTitles = {"Отправленные", "Принятые", "Отказаные"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        ButterKnife.bind(this);

        mPagerAdapter = new OrderPagerAdapter(getSupportFragmentManager(), mPageTitles);
        mViewPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @OnClick(R.id.toolbar_back_button)
    void onToolbarBackClicked() {
        onBackPressed();
    }
}
