package com.example.railwaycargo.ui.main.home_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.railwaycargo.R;
import com.example.railwaycargo.data.network.entity.OrdersResponse;
import com.example.railwaycargo.data.network.entity.StationsResponse;
import com.example.railwaycargo.ui.main.callback.MainActivityListener;
import com.example.railwaycargo.ui.widgets.adapters.OrdersAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class HomeFragment extends MvpAppCompatFragment implements HomeView, OrdersAdapter.OrderItemClickListener {

    @BindView(R.id.recycler_view)
    RecyclerView mOrdersRecyclerView;
    @BindView(R.id.progress_view)
    View mProgressView;

    @InjectPresenter
    HomePresenter mHomePresenter;

    private MainActivityListener mListener;
    private OrdersAdapter mOrdersAdapter;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (MainActivityListener) context;
        mListener.changeTitleText("Информация");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mHomePresenter.getOrders();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        mOrdersAdapter = new OrdersAdapter(getActivity());
        mOrdersAdapter.setItemClickListener(this);
        mOrdersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void showNoInternetConnection() {

    }

    @Override
    public void showProgress() {
        mProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressView.setVisibility(View.GONE);
    }


    @Override
    public void setOrdersList(List<OrdersResponse.Order> ordersList) {
        mOrdersAdapter.setOrdersList(ordersList);
    }

    @Override
    public void setStationsList(List<StationsResponse.Station> stationsList) {
        mOrdersAdapter.setStationsList(stationsList);
        mOrdersRecyclerView.setAdapter(mOrdersAdapter);
    }

    @Override
    public void loadError(String message) {

    }

    @Override
    public void onOrderItemClick(int position) {

    }
}
