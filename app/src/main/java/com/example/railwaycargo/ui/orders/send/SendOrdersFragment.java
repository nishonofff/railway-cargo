package com.example.railwaycargo.ui.orders.send;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.railwaycargo.R;
import com.example.railwaycargo.ui.orders.order_info.OrderInfoActivity;
import com.example.railwaycargo.ui.widgets.adapters.OrdersAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public class SendOrdersFragment extends Fragment implements OrdersAdapter.OrderItemClickListener {


    @BindView(R.id.send_orders_recycler_view)
    RecyclerView mRecyclerView;

    public static final String TAG = SendOrdersFragment.class.getName();

    public static SendOrdersFragment newInstance() {
        SendOrdersFragment fragment = new SendOrdersFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_send_orders, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        OrdersAdapter adapter = new OrdersAdapter(getActivity());
        adapter.setItemClickListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(adapter);

    }

    @Override
    public void onOrderItemClick(int position) {
        Intent intent = new Intent(getActivity(), OrderInfoActivity.class);
        startActivity(intent);
    }
}
