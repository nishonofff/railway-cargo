package com.example.railwaycargo.ui.history;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.railwaycargo.R;
import com.example.railwaycargo.ui.widgets.adapters.HistoryPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HistoryActivity extends AppCompatActivity {

    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    private HistoryPagerAdapter mPagerAdapter;

    String[] mPageTitles = {"Активные", "Завершенные"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        ButterKnife.bind(this);

        mPagerAdapter = new HistoryPagerAdapter(getSupportFragmentManager(), mPageTitles);
        mViewPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @OnClick(R.id.toolbar_back_button)
    void onToolbarBackClicked() {
        onBackPressed();
    }
}
