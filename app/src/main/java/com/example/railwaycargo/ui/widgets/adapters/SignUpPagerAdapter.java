package com.example.railwaycargo.ui.widgets.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.railwaycargo.ui.auth.sign_up_activity.sign_up_fragment.SignUpFragment;
import com.example.railwaycargo.ui.user_account.balance.BalanceFragment;
import com.example.railwaycargo.ui.user_account.info.UserInfoFragment;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */

public class SignUpPagerAdapter extends FragmentPagerAdapter {

    public SignUpPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return SignUpFragment.newInstance(position);
            case 1:
                return SignUpFragment.newInstance(position);
            case 2:
                return SignUpFragment.newInstance(position);
            default:
                return SignUpFragment.newInstance(position);
        }
    }
}
