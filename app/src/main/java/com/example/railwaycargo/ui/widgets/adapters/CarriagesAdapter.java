package com.example.railwaycargo.ui.widgets.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.railwaycargo.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class CarriagesAdapter extends RecyclerView.Adapter<CarriagesAdapter.ViewHolder> {

    private Context mContext;

    private static CarriageItemClickListener mItemClickListener;

    public void setItemClickListener(CarriageItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public CarriagesAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.carriage_item, parent, false), mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setHistoryItem();
    }

    @Override
    public int getItemCount() {
        return 20;
    }

    public interface CarriageItemClickListener {
        void onCarriageItemClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        CarriageItemClickListener mItemClickListener;

        public ViewHolder(@NonNull View itemView, CarriageItemClickListener itemClickListener) {
            super(itemView);
            mItemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
        }

        public void setHistoryItem() {

        }

        @OnClick(R.id.carriage_item)
        void onCarriageClicked(){
            mItemClickListener.onCarriageItemClick(getAdapterPosition());
        }

    }
}
