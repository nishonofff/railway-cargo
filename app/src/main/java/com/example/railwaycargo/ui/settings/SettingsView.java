package com.example.railwaycargo.ui.settings;

import com.arellomobile.mvp.MvpView;

public interface SettingsView extends MvpView {

    void showNoInternetConnection();

}
