package com.example.railwaycargo.ui.widgets.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.railwaycargo.R;
import com.example.railwaycargo.data.network.entity.OrdersResponse;
import com.example.railwaycargo.data.network.entity.StationsResponse;
import com.example.railwaycargo.utils.AppUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

    private Context mContext;

    private static OrderItemClickListener mItemClickListener;

    private List<OrdersResponse.Order> mOrdersList;

    private List<StationsResponse.Station> mStationsList;

    public OrdersAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setItemClickListener(OrderItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public void setOrdersList(List<OrdersResponse.Order> ordersList) {
        this.mOrdersList = ordersList;
    }

    public void setStationsList(List<StationsResponse.Station> stationsList) {
        this.mStationsList = stationsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item, parent, false), mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setOrderItem(mOrdersList.get(i));
    }

    @Override
    public int getItemCount() {
        if (mOrdersList != null)
            return mOrdersList.size();
        else return 0;
    }

    public interface OrderItemClickListener {
        void onOrderItemClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.order_date)
        TextView mOrderDateView;
        @BindView(R.id.order_address)
        TextView mOrderAddressView;

        OrderItemClickListener mItemClickListener;

        public ViewHolder(@NonNull View itemView, OrderItemClickListener itemClickListener) {
            super(itemView);
            mItemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
        }

        public void setOrderItem(OrdersResponse.Order orderItem) {
            mOrderDateView.setText(String.format("%s - %s", AppUtils.formatDateToLocal(orderItem.startDate), AppUtils.formatDateToLocal(orderItem.endDate)));
            String startStation = null, endStation = null;
            for (StationsResponse.Station station : mStationsList) {
                if (station.id == orderItem.from)
                    startStation = station.name;
                if (station.id == orderItem.to)
                    endStation = station.name;
            }

            mOrderAddressView.setText(String.format("%s - %s", startStation, endStation));

        }

        @OnClick(R.id.order_item)
        void onOrderClicked() {
            mItemClickListener.onOrderItemClick(getAdapterPosition());
        }
    }
}
