package com.example.railwaycargo.ui.auth.callback;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

public interface SyncFragmentCallback {

    void showConfirmProgress();

    void hideConfirmProgress();

    void syncSuccess(String message);
}
