package com.example.railwaycargo.ui.splash_screen;

import android.annotation.SuppressLint;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.railwaycargo.App;
import com.example.railwaycargo.data.network.ApiService;
import com.example.railwaycargo.data.prefs.PrefsService;

import javax.inject.Inject;

@InjectViewState
public class SplashScreenPresenter extends MvpPresenter<SplashScreenView> {
    private static final String TAG = SplashScreenPresenter.class.getName();

    @Inject
    ApiService mApiService;

    @Inject
    PrefsService mPrefsService;

    SplashScreenPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void checkUserStatus() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            Log.w(TAG, "No internet connection");
            return;
        }

        if (!mPrefsService.loggedIn())
            getViewState().setUserStatus(false);

        else
            getViewState().setUserStatus(true);
    }

}
