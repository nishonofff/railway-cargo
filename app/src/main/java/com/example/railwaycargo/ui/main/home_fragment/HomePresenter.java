package com.example.railwaycargo.ui.main.home_fragment;

import android.annotation.SuppressLint;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.railwaycargo.App;
import com.example.railwaycargo.data.network.ApiService;
import com.example.railwaycargo.data.network.entity.OrdersResponse;
import com.example.railwaycargo.data.network.entity.StationsResponse;
import com.example.railwaycargo.data.prefs.PrefsService;

import java.util.List;

import javax.inject.Inject;

@InjectViewState
public class HomePresenter extends MvpPresenter<HomeView> {

    private static final String TAG = HomePresenter.class.getName();

    @Inject
    ApiService mApiService;

    @Inject
    PrefsService mPrefsService;

    HomePresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void getOrders() {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            Log.w(TAG, "No internet connection");
            return;
        }
        getViewState().showProgress();

        mApiService.getGOrders(mPrefsService.getPhoneNumber("phone")).subscribe((List<OrdersResponse.Order> response) -> {

            getViewState().setOrdersList(response);

        }, (Throwable throwable) -> {

        });

        mApiService.getStations().subscribe((List<StationsResponse.Station> response) -> {
            getViewState().hideProgress();
            getViewState().setStationsList(response);

        }, (Throwable throwable) -> {
            getViewState().hideProgress();
        });



    }
}
