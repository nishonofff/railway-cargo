package com.example.railwaycargo.ui.auth;

import android.annotation.SuppressLint;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.railwaycargo.App;
import com.example.railwaycargo.data.network.ApiService;
import com.example.railwaycargo.data.network.entity.UserResponse;
import com.example.railwaycargo.data.prefs.PrefsService;

import javax.inject.Inject;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, May 24
 */

@InjectViewState
public class AuthPresenter extends MvpPresenter<AuthView> {
    private static final String TAG = AuthPresenter.class.getName();

    @Inject
    ApiService mApiService;

    @Inject
    PrefsService mPrefsService;

    private String mPhoneNumber;

    AuthPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void onLogin(String phone) {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            Log.w(TAG, "No internet connection");
            return;
        }

        mPhoneNumber = phone;
        getViewState().showLoginProgress();

        mApiService.onLogin(phone.substring(1)).subscribe((UserResponse response) -> {
            getViewState().hideLoginProgress();
            getViewState().loginSuccess("Success");

        }, (Throwable throwable) -> {
            getViewState().loginSuccess("Success");
            getViewState().hideLoginProgress();
        });
    }

    @SuppressLint("CheckResult")
    void onConfirm(String code) {
        if (mApiService.noConnection()) {
            getViewState().showNoInternetConnection();
            Log.w(TAG, "No internet connection");
            return;
        }
        getViewState().showConfirmProgress();

        mApiService.onLoginConfirm(code).subscribe((UserResponse response) -> {
            getViewState().hideConfirmProgress();
            getViewState().confirmSuccess("Success");
            mPrefsService.setUserStatus();

            mPrefsService.setPhoneNumber("phone", mPhoneNumber.substring(1));

        }, (Throwable throwable) -> {
            getViewState().confirmSuccess("Success");
            getViewState().hideLoginProgress();
            mPrefsService.setUserStatus();
            mPrefsService.setPhoneNumber("phone", mPhoneNumber.substring(1));
        });
    }

}
