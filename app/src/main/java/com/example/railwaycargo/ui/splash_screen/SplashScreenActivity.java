package com.example.railwaycargo.ui.splash_screen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.railwaycargo.R;
import com.example.railwaycargo.ui.auth.AuthActivity;
import com.example.railwaycargo.ui.main.MainActivity;

/**
 * @author Ikromjon Nishonofff
 * @since 2019, March 28
 */
public class SplashScreenActivity extends MvpAppCompatActivity implements SplashScreenView {

    private SplashScreenActivity mContext;

    @InjectPresenter
    SplashScreenPresenter mSplashScreenPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        mContext = this;

        mSplashScreenPresenter.checkUserStatus();
    }

    @Override
    public void showNoInternetConnection() {

    }

    @Override
    public void setUserStatus(boolean loggedIn) {

        Intent intent;

        if (loggedIn)
            intent = new Intent(mContext, MainActivity.class);
        else
            intent = new Intent(mContext, AuthActivity.class);

        int SPLASH_SCREEN_DURATION = 1500;
        new Handler().postDelayed(() -> {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        }, SPLASH_SCREEN_DURATION);
    }
}
