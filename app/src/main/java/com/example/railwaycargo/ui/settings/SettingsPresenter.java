package com.example.railwaycargo.ui.settings;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.railwaycargo.App;
import com.example.railwaycargo.data.network.ApiService;
import com.example.railwaycargo.data.prefs.PrefsService;
import com.example.railwaycargo.ui.splash_screen.SplashScreenView;

import javax.inject.Inject;

@InjectViewState
public class SettingsPresenter extends MvpPresenter<SettingsView> {
    private static final String TAG = SettingsPresenter.class.getName();

    @Inject
    ApiService mApiService;

    @Inject
    PrefsService mPrefsService;

    SettingsPresenter() {
        App.getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    void logOut() {
//        if (mApiService.noConnection()) {
//            getViewState().showNoInternetConnection();
//            Log.w(TAG, "No internet connection");
//            return;
//        }

        mPrefsService.clearUserData();
    }

}
